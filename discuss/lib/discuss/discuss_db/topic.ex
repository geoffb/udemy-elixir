defmodule DiscussDb.Repo.Topic do
  alias Discuss.{Repo, Topic}

  def get_empty_topic() do
    %Topic{}
  end

  def get() do
    Repo.all(Topic)
  end

  def get!(id, preload \\ []) do
	  Repo.get!(Topic, id) |> Repo.preload(preload)
	end 

  def get_changeset(topic, attrs) do
    Topic.changeset(topic, attrs)
  end

  def create(topic, user) do
    user
    |> Ecto.build_assoc(:topics)
    |> get_changeset(topic)
    |> Repo.insert()
  end

  def update(changeset) do
    Repo.update(changeset)
  end

  def delete!(topic) do
    Repo.delete!(topic)
  end
end
