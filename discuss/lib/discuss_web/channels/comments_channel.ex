defmodule DiscussWeb.CommentsChannel do
  use DiscussWeb, :channel

  alias DiscussDb.Repo.Topic
  alias Discuss.Comment
  alias Discuss.Repo

  def join("comments:" <> topic_id, _params, socket) do
    topic_id = String.to_integer(topic_id)
    topic = Topic.get!(topic_id, [comments: [:user]])

    {:ok, %{comments: topic.comments}, assign(socket, :topic, topic)}
  end

  def handle_in(name, message, socket) do
    case name do
      "comment:hello" ->
        IO.inspect(message)
        {:reply, :ok, socket}
      "comment:add" ->
        %{"content" => content} = message
        topic = socket.assigns.topic
        user_id = socket.assigns.user_id

        changeset = topic
          |> Ecto.build_assoc(:comments, user_id: user_id)
          |> Comment.changeset(%{content: content})

        case Repo.insert(changeset) do
          {:ok, comment} ->
            # If we pass directly the comment like that we get
            # cannot encode association :user from Discuss.Comment to JSON because the association was not loaded.
            # need to fetch again to preload the user
            new_comment = Repo.get!(Comment, comment.id) |> Repo.preload([:user])
            broadcast!(socket, "comments:#{socket.assigns.topic.id}:new", %{comment: new_comment})
            {:reply, :ok, socket}
          {:error, _reason} ->
            {:reply, {:error, %{errors: changeset}}, socket}
        end
    end
  end
end