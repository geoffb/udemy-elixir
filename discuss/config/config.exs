# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :discuss,
  ecto_repos: [Discuss.Repo]

# Configures the endpoint
config :discuss, DiscussWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "c1oQqgGPP8Vl3GXOpa5X3NJTLHX+Q8dDkBXqyWJs6fpE/b3dvJVZSgh07R2LsQHD",
  render_errors: [view: DiscussWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Discuss.PubSub,
  live_view: [signing_salt: "h/l4sWym"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# UeberAuth
config :ueberauth, Ueberauth,
  providers: [
    identity: { Ueberauth.Strategy.Identity, [
        callback_methods: ["POST"],
        uid_field: :email,
        nickname_field: :username,
      ] },
    gitlab: {Ueberauth.Strategy.Gitlab, [default_scope: "api read_user"]},
    twitter: {Ueberauth.Strategy.Twitter, []}
  ]

config :ueberauth, Ueberauth.Strategy.Gitlab.OAuth,
  client_id: "8a6b22d276c5efb7a49bb4f99933c557d4bd9978ae2291f65fa03b1e27e897e7",
  client_secret: "3b5e42b8740f4ece00e2516f6c9d6c0bbf0248887d748d4914e33897c6c12e44",
  redirect_uri: "http://localhost:4000/auth/gitlab/callback"

config :ueberauth, Ueberauth.Strategy.Twitter.OAuth,
  consumer_key: "9GScfK17YAKGGJ2t3c72ENNyl",
  consumer_secret: "DiP1j3I2rRVm4TelS7tez6eBs6niXFBhB79c9TJJcVL7tueMhN"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
