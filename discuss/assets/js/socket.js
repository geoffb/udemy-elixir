import {Socket} from "phoenix"

let socket = new Socket("/socket", {params: {token: window.userToken}})
let channel

socket.connect()

const createSocket = (topicId) => {
  channel = socket.channel(`comments:${topicId}`, {})
  channel.join()
    .receive("ok", resp => {
      console.log("Joined successfully", resp)
      renderComments(resp.comments)
    })
    .receive("error", resp => { console.log("Unable to join", resp) })

  document.querySelector('button').addEventListener('click', () => {
    const content = document.querySelector('textarea').value
    channel.push('comment:add', {content: content})
  })

  channel.on(`comments:${topicId}:new`, renderComment)
}

const ping = () => {
  channel.push('comment:hello', {hi: "there"})
  console.log("Ping sent")
}

const renderComments = comments => {
  console.log(comments)
  const renderedComments = comments.map(comment => {
    return commentTemplate(comment)
  })

  document.querySelector('.collection').innerHTML = renderedComments.join('')
}

const renderComment = eventObject => {
  document.querySelector('.collection').innerHTML += commentTemplate(eventObject.comment)
}

const commentTemplate = comment => {
  const defaultEmail = 'Anonymous'
  return `
    <li class="collection-item">
      ${comment.content}
      <div class="secondary-content">
        ${comment.user ? comment.user.email : defaultEmail}
      </div>
    </li>`
}

window.ping = ping
window.createSocket = createSocket
