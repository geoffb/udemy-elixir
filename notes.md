## Install Elixir 

	$ brew install elixir

## Test elixir is working
	
	$ elixir

	asdf: No version set for command elixir you might want to add one of the following in your .tool-versions file:
	elixir 1.10.2-otp-22

I had to create a .tool-versions and add the following elixir version output  
Then it was fixed

## Generating a project

Using CLI mix [Internal piece which can: Create project, Compile, Run tasks, Manage deps]

	$ mix new cards

	asdf: No version set for command erl
	you might want to add one of the following in your .tool-versions file:

	erlang 22.2.8
	erlang 22.3.3

Again, I had to add one of those versions

## Modules

Collection of methods/functions

	defmodule Cards do
		def hello do
			:world
		end
	end

The last value of an elixir module will always be the "default" return

## Run the cards module

Interactive elixir shell: can write elixir code
	
	$ iex -S mix 


	iex(1)> 2 + 2
	4
	iex(2)> Cards
	Cards
	iex(3)> Cards.hello
	:world
	iex(4)> 

## List and strings
	def create_deck do
		["Ace", "Two", "Three"]
	end

The norme is to use double quotes (single quotes are supported but are not the standard)  
If we try to run the function in iex now it will say undefined or private: recompile 

	iex(4)> recompile
	Compiling 1 file (.ex)
	:ok

	iex(5)> Cards.create_deck
	["Ace", "Two", "Three"]

## Object Oriented vs Functional Programming

No objects, or instances

## Method arguments

/1 /2 - herety 1, herety 2

	iex(6)> recompile 
	Compiling 1 file (.ex)
	warning: variable "deck" is unused (if the variable is not meant to be used, prefix it with an underscore)
	  lib/cards.ex:6: Cards.shuffle/1

	:ok
	iex(7)> Cards.shuffle
	** (UndefinedFunctionError) function Cards.shuffle/0 is undefined or private. Did you mean one of:

	      * shuffle/1

	    (cards 0.1.0) Cards.shuffle()

## Immutability
	
	deck = Cards.create_deck
	Cards.shuffle(deck) will return another array
	
Everything that modifies the data structure will return a new version of that data

## Methods name

Method name can have a question mark -> easy to read if method returns a boolean  
Convention for returning true/false

## for loops and arrays

	values = ["one", "two", "three"]

	for value <- values do
		"#{value} is from my array"
	end

The result will be return inside of a new array.  
The comprehension is a map., a mapping function   
The result of this comprehension will be an array

Nested array problem:

	def create_deck do
		values = ["Ace", "Two", "Three", "Four", "Five"]
    suits = ["Spades", "Clubs", "Hearts", "Diamonds"]

    for value <- values do
      for suit <- suits do
        "#{value} of #{suit}"
      end
    end
	end

This will return an array of four new arrays  
Because after each do block, the comprehension returns an array

	iex(14)> Cards.create_deck
	[
	  ["Ace of Spades", "Ace of Clubs", "Ace of Hearts", "Ace of Diamonds"],
	  ["Two of Spades", "Two of Clubs", "Two of Hearts", "Two of Diamonds"],
	  ["Three of Spades", "Three of Clubs", "Three of Hearts", "Three of Diamonds"],
	  ["Four of Spades", "Four of Clubs", "Four of Hearts", "Four of Diamonds"],
	  ["Five of Spades", "Five of Clubs", "Five of Hearts", "Five of Diamonds"]
	]


Solutions:

- List.flatten: takes all the the arrays inside an array and 'merge' them together.

		cards = for value <- values do
	      for suit <- suits do
	        "#{value} of #{suit}"
	      end
	    end

	    List.flatten(cards)

	    iex(15)> recompile        
		Compiling 1 file (.ex)
		:ok
		iex(16)> Cards.create_deck
		["Ace of Spades", "Ace of Clubs", "Ace of Hearts", "Ace of Diamonds",
		 "Two of Spades", "Two of Clubs", "Two of Hearts", "Two of Diamonds",
		 "Three of Spades", "Three of Clubs", "Three of Hearts", "Three of Diamonds",
		 "Four of Spades", "Four of Clubs", "Four of Hearts", "Four of Diamonds",
		 "Five of Spades", "Five of Clubs", "Five of Hearts", "Five of Diamonds"]

- Passing two collections to the comprehension:

		for suit <- suits, value <- values do
	      "#{value} of #{suit}"
	    end

This is much appropriate and shorter.

## Deal method:

	We used before:

	- Enum.shuffle 
	- Enum.member?

	Now we will use Enum.split(deck, hand_size)
	This will return a tuple {hand, rest}, hand is always index 0 and the rest is always index 1

	How to get access to the element of the tuple:
	Pattern matching is elixir's replacement for variable assignment

	{ hand, rest_of_deck } = Cards.deal(deck, hand_size)

	Everytime we see '=' is pattern matching

> Elixir vs Erlang

	Code we write 
		Get fed into
	Elixir
		Transpiled into
	Erlang
		Complied and executed by
	BEAM


	Elixir is a better interface to Erlang
	BEAM is like JVM is for Java

> Writting to the filesystem

	We need to write a little bit of Erlang code

	def save(deck, filename) do
		binary = :erlang.term_to_binary(deck)
		File.write(filename, binary)
	end

	iex(25)> Cards.save(deck, "my_deck")
	:ok

> Load the deck from filesystem

	def load(filename) do
	    {_status, binary} = File.read(filename)
	    :erlang.binary_to_term(binary)
	end

	Underscore is for unused variables
	Colon + string is a symbol or atom (:error, :enoent) (No entity)

> Handling the case if the file is not found

	Avoid writting if at all cost

	  def load(filename) do
	    {status, binary} = File.read(filename)

	    case status do
	      :ok -> :erlang.binary_to_term(binary)
	      :error -> "That file does not exists"
	    end
	  end

	It works but we can dense the code:

	  def load(filename) do
	    case File.read(filename) do
	      {:ok, binary} -> :erlang.binary_to_term(binary)
	      {:error, _reason} -> "That file does not exists"
	    end
	  end

> Pipe operator

	|>  Gets the result of the obove line and passes it to the current line

	  def create_hand(hand_size) do
	    Cards.create_deck
	    |> Cards.shuffle
	    |> Cards.deal(hand_size) 
	  end

	Cards.deal takes two arguments but only one is passed. 
	The pipe operator will pass the argument automatically to the first parameter and we don't have to specify it.

> Dependencies and documentation

	Inside the mix.exs there is a deps array which contains the list of all the denpencies
	To add a dep we need to manually add it to this file.
	It will be a tuple with the 1st arg as the name of the package and the second is the version:

	{:ex_doc, "~> 0.22"}

	To install it we need to run a command:
	$ mix deps.get

	2 documentations
	- Module documentation

	  @moduledoc """
	    Provides methods for creating and handling a deck of cards
	  """

	To generate the documentation:
	$ mix docs

	Then in the dov directory there is an index.html file

	- Function documentation

	  @doc """
	    Returns a list of strings representing a deck of playing cards
	  """

> Testing

	When generating a project with mix, it creates a test directory automatically
	Doctest!

	To test;
	$ mix test

	If there is no test but there is an example in the documentation.
	Elixir will test this block of code (1 doctest)

	$ mix test
	.

	Finished in 0.03 seconds
	1 doctest, 0 failures

	It is very important to have the exact formatting:
	It will look for "## Examples"

	  @doc """
	    Determines wether a deck contains a given card

	  ## Examples

	      iex> deck = Cards.create_deck
	      iex> Cards.contains?(deck, "Ace of Spades")
	      true    

	  """

	Create a test statement:
	  test "create_deck makes 20 cards" do
	    deck_length = length(Cards.create_deck)
	    assert deck_length == 20
	  end

> Maps
	https://hexdocs.pm/elixir/Map.html

	Is like a javascript object
	colors = %{primary: "red", secondary: "blue"}

	Works great with pattern matching:
	%{secondary: secondary_color} = colors

	iex> secondary_color
	"blue"

	Updating a map value:
	Map.put(colors, :primary, "blue")

	But this creates a new Map, it does not change the original one

	This does the same thing, it updates an existing key. It doesn't work with a new key:
	%{colors | primary: "blue"}

	Add a property:
	Map.put(colors, :secondary_color, "green")

> Lists
	https://hexdocs.pm/elixir/List.html

	A list of tuples:
	colors = [{:primary, "red"}, {:secondy, "green"}]

	iex> colors[:primary]
	"red"

	This also works:
	colors = [primary: "red", secondary: "blue"]

	So what is the difference with Maps? 
	With maps we can only have one property instance per map (primary for ex)
	With list we can declare many "primary"
	But why would we use that? Apparently Ecto (library made for working with db)

	Ex: query = Users.find_where([where: user.age > 10, where: user.subscribed == true])

	There is one more shortcut with Lists
	If the last argument is a keyword list we can remove the square brackets:
	Like the example above: 
	query = Users.find_where(where: user.age > 10, where: user.subscribed == true)

	We can also omit the parenthesis: 
	query = Users.find_where where: user.age > 10, where: user.subscribed == true

> New project
	
	$ mix new identicon

	  def main(input) do
	    input
	    |> hash_input
	  end

	The first line of the pipe can just be the plain value and it will be passed down the pipe. As the first arg of hash_input

> Hash a string

	  def hash_input(input) do
	    :crypto.hash(:md5, input)
	    |> :binary.bin_to_list
	  end

	iex(5)> Identicon.hash_input("geoffroy")
	[192, 152, 188, 27, 181, 110, 185, 26, 4, 153, 62, 79, 250, 241, 101, 192]

	Don't forget we can omit the parenthesis for the args:
	iex(6)> Identicon.hash_input "geoffroy"
	[192, 152, 188, 27, 181, 110, 185, 26, 4, 153, 62, 79, 250, 241, 101, 192]


> Struct

	Struct is a better map
	Default properties
	To define a struct we need a new module

	defmodule Identicon.Image do
	  defstruct hex: nil 
	end

	To create an instance of this struct:
	iex> %Identicon.Image{}

	The only properties that can exist in a struct are the ones defined in the file.
	Maps don't behave this way

	  def hash_input(input) do
	    hex = :crypto.hash(:md5, input)
	    |> :binary.bin_to_list

	    %Identicon.Image{hex: hex}
	  end

	iex(3)> Identicon.main("asdf")
	%Identicon.Image{
	  hex: [145, 46, 200, 3, 178, 206, 73, 228, 165, 65, 6, 141, 73, 90, 181, 112]
	}

	In this Image.ex file there will be no method.
	In functional programming there is no such thing
	No possibility to attach methods, under the hood is a map

> Pattern matching struct
	
	We can not get the first element of an array like image[0]
	We need to use pattern matching:
	
	%Identicon.Image{hex: hex_list} = image

	Now we can use hex_list

	  def main(input) do
	    input
	    |> hash_input
	    |> pick_color
	  end

	  def pick_color(image) do
	    %Identicon.Image{hex: hex_list} = image
	    hex_list
	  end

	iex(5)> Identicon.main("asdf")
    [145, 46, 200, 3, 178, 206, 73, 228, 165, 65, 6, 141, 73, 90, 181, 112]

	How to get the first three elements of hex_list

	If we match only r, g, b there will be an error because it doesn't match the whole array.

	To tell elixir to match the whole array use : "| _tail" or whatever name you want to give

	  def pick_color(image) do
	    %Identicon.Image{hex: hex_list} = image
	    [r, g, b | _tail] = hex_list
	  end

	Can be shortened by:

	  def pick_color(image) do
	    %Identicon.Image{hex: [r, g, b | _tail]} = image
	  end

> Update the struct

	We have a new property color in our struct:

	defmodule Identicon.Image do
	  defstruct hex: nil, color: nil
	end

	And we want to update it it's value. 
	Remember everytime it's a new object that is created, so it's exactly what we will do.

	  def pick_color(image) do
	    %Identicon.Image{hex: [r, g, b | _tail]} = image
	    %Identicon.Image{image | color: [r, g, b]}
	  end

	It is possible to pattern match inside the parameters.
	So our last code can be refactor into:

	  def pick_color(%Identicon.Image{hex: [r, g, b | _tail]} = image) do
	    %Identicon.Image{image | color: [r, g, b]}
	  end

	And if we had another parameter it will also be possbile to pattern match it.

> Building the grid

	|> Enum.map(&mirror_row/1)

	The & tells elixir we are passigna reference to the function
	/1 tells how many args we want, we need to specify the number of arguments to the function.

> Grid structure

	When we use a function like Enum.filter which takes a function parameter like so:

	Enum.filter(grid, fn(square) -> end)

	The convention is to write a cleaner code like this:

	Enum.filter grid, fn(square) -> 
		#do things
	end

	rem() calculates the remaining amount
	rem(19, 9) = 1

> Create an image with erlang

	egd = graphical library for erlang
	egd.create()

	agd was undefined for me I had to install it
	https://codeselfstudy.com/blog/how-to-install-erlangs-egd-library-in-elixir/

	in mix.exs:
	{:egd, github: "erlang/egd"}

	Then run mix deps.get

> Full code

	- image.ex
		defmodule Identicon.Image do
		  defstruct hex: nil, color: nil, grid: nil, pixel_map: nil
		end

	- identicon.ex
		defmodule Identicon do
		  def main(input) do
		    input
		    |> hash_input
		    |> pick_color
		    |> build_grid
		    |> filter_odd_squares
		    |> build_pixel_map
		    |> draw_image
		    |> save_image(input)
		  end

		  def hash_input(input) do
		    hex =
		      :crypto.hash(:md5, input)
		      |> :binary.bin_to_list

		    %Identicon.Image{hex: hex}
		  end

		  def pick_color(%Identicon.Image{hex: [r, g, b | _tail]} = image) do
		    %Identicon.Image{image | color: {r, g, b}}
		  end

		  def build_grid(%Identicon.Image{hex: hex} = image) do
		    grid = 
		      hex
		      |> Enum.chunk(3)
		      |> Enum.map(&mirror_row/1)
		      |> List.flatten
		      |> Enum.with_index

		    %Identicon.Image{image | grid: grid}
		  end

		  def mirror_row(row) do
		    # [145, 46, 200]
		    [first, second | _tail] = row

		    # [145, 46, 200, 46, 145]
		    row ++ [second, first]
		  end

		  def filter_odd_squares(%Identicon.Image{grid: grid} = image) do
		    grid = Enum.filter grid, fn({code, _index}) -> 
		      rem(code, 2) == 0
		    end

		    %Identicon.Image{image | grid: grid}
		  end

		  def build_pixel_map(%Identicon.Image{grid: grid} = image) do
		    pixel_map = Enum.map grid, fn({_code, index}) ->
		      horizontal = rem(index, 5) * 50
		      vertical = div(index, 5) * 50

		      top_left = {horizontal, vertical}
		      bottom_rigth = {horizontal + 50, vertical + 50}

		      {top_left, bottom_rigth}
		    end

		    %Identicon.Image{image | pixel_map: pixel_map}
		  end

		  def draw_image(%Identicon.Image{color: color, pixel_map: pixel_map}) do
		    image = :egd.create(250, 250)
		    fill = :egd.color(color)

		    Enum.each pixel_map, fn({start, stop}) ->
		      :egd.filledRectangle(image, start, stop, fill)
		    end

		    :egd.render(image)
		  end

		  def save_image(image, input) do
		    File.write("#{input}.png", image)
		  end
		end

> Phoenix!
	
	https://hexdocs.pm/phoenix/installation.html#content
	$ mix archive.install hex phx_new 1.5.3

	Install Postgres
	brew update
	brew install postgresql
	brew services start postgresql
	initdb /usr/local/var/postgres/

> About Phoenix

	Phoenix is to behave as a webserver

	HTML JSON WebSocket
		   |
		-------
	   |Phoenix|
		-------
		   |
		Database

> How Phoenix works behind the scene

	- Incoming request
	- Ensure its an HTML request
	- See if it has a session
	- Do a security check
	- Put on HTTP headers for a browser
	- See what the request was trying to access
	- Formulate and return a request

> Generate a new Phoenix project

	$ mix phoenix.new discuss

	Not working with version I use

	$ mix phx.new discuss

	And install dependencies "Y"
	Which does mix deps.get

	Done!

	We are almost there! The following steps are missing:
	    $ cd discuss

	Then configure your database in config/dev.exs and run:
	    $ mix ecto.create

	Start your Phoenix app with:
	    $ mix phx.server

	You can also run your app inside IEx (Interactive Elixir) as:
	    $ iex -S mix phx.server

> Create database

	$ mix ecto.create
	ERROR role "postgres" does not exist

	This is due to Homebrew installation. It doesn't create a user postgres.

	Go to /config/dev.exs
	And change user to $ whoami
	And remove password

	And now:
	$ mix ecto.create
	Compiling 14 files (.ex)
	Generated discuss app
	The database for Discuss.Repo has been created

> Start the server

	$ mix phx.server

	Change the template page in: /lib/templates/page/index.html.eex

> SST: Server side templating

	When user clicks on a link on the page it send the request to the server and recreate a new document.
	Against Single Page App

> Templates vs Layout

	.eex = elixir template file

	Layout is used for every single page, headers for example
	Template

	By default Phoenix uses Bootstrap

	Now let's add material design: and use 
	https://materializecss.com/

> MVC

	Phoenix uses MVC architecture

> How a request goes through phoenix

	Request > Router > Controller [Database > Model > Controller] > View [Template > View] > Response

	In /lib/discuss_web/router:

	get "/", PageController, :index

	This watches a GET request to / and sends it to the index function of the PageController

> Views vs Template

	Views folder contains PageView module

	Templates folder contains page folder with a page template file

	When phoenix get a request it looks for the name of the Module
	Here "Page" it dumps the "view part"

		defmodule DiscussWeb.PageView do
		  use DiscussWeb, :view
		end

	Phoenix will then look inside the template folder and will try to find a folder named Page

	Because the name of the view module and the name of the folder in template match
	It will get all the files inside of the template folder and it will pass it as a function to the page view
	Specifically a function render("index.html")

	WE NEED TO FOLLOW NAME CONVENTION

> Debugging phoenix in console

	Make sure to stop the server ctrl+c
	THen start an iex session with passing the phoenix server as an argument

	$ iex -S mix phx.server

	But because of webpack, it doesn't really work, there must be another solution

> Models

	When we set up the db, it is empty
	We need to give Postgres what is called Migrations

> Migration files

	We have to instruct Postgres about the architecture of our database
	A Migration is when we change the structure of our db

	To execute a migration, we make a migration file

	In the CLI, we can generate a migration file to add the topic table

	$ mix ecto.gen.migration add_topics

	It creates a /priv/repo/migrations/[timestamp]_add_topics.exs
	It is prepended by a timestamp because then phoenix will run each migration in the right order

	defmodule Discuss.Repo.Migrations.AddTopics do
	  use Ecto.Migration

	  def change do
	    create table(:topics) do
	      add :title, :string
	    end 
	  end
	end

	Create a new table called topics
	With a column called title with a type of string

	To run the migrations:
	$ mix ecto.migrate

	09:07:14.835 [info]  == Running 20200716070125 Discuss.Repo.Migrations.AddTopics.change/0 forward
	09:07:14.837 [info]  create table topics
	09:07:14.869 [info]  == Migrated 20200716070125 in 0.0s

	To check, we can use Postico a Postgres GUI

	localhost:5432
	geoffroy_baumier
	discuss_dev
	[no password]

> Start and stop the project

	The Udemy class is getting in conflict with Favur. There are both using postgres on the same port.

	To start the project:

	- Start postgres:
		$ brew services start postgres

	- Start the server:
		$ mix phx.server

	To stop the project:

	- To stop the server:
		$ ctrl + c

	- To stop postgres:
		$ brew services stop postgres

> Phoenix server process

	- Need a new url for the user to visit
		=> Add a new route in the router

	- New routes must map up to a method in a controller
		=> Add a new method in a controller to handle the request. The method will decide what to do with the request

	- Need to show a form to the user
		=> Make a new template that contains the form

	- Need to translate data in the form
		=> Create a 'Topic' model that can translate raw data from the form into something that can be savec to the db 

	- The controller and view that we currently have are related to 'Page', but we are making stuff related to 'Topic'
		=> Make a new controller and view to handle everything related to 'Topics'

> Adding a new route

	get "/topics/new", TopicController, :new

> Create a new controller

	Create a new file inside the controller folder

	defmodule DiscussWeb.TopicController do
	  use DiscussWeb, :controller

	  def new(conn, _params) do
	    render(conn, "index.html")
	  end
	end

> Code reuse: import, alias, use
	
	There is no inheritance in Phoenix
	To reuse code let's use those keyword:

		- import: Take all the functions out of this module and give them to this other module
		- alias: Give me a shortcut to this other module, my fingers are lazy
		- use: I want to do some really really fancy setup 

	Import:

		defmodule Math do
			def add(a, b), do: a + b
		end

		defmodule TopicController do
			import Math

			def log(), do: IO.puts "hey"
		end

		TopicController.add(1, 2)
		# 3

			TopicController now has all of the methods in Math

	Alias:

		defmodule Math do
			def add(a, b), do: a + b
		end

		defmodule TopicController do
			alias Math

			def log(), do: add(1, 2)
		end

		TopicController.log(1, 2)
		# 3

		TopicController.add(1, 2)
		# BOOM

			What elixir does here, it will first look for a function add inside the TopicController then if it doesn't look in Math.

			We could also have done:
			defmodule TopicController do
				def log(), do: Math.add(1, 2)
			end

			It is just saving a few keystokes "Math."

	Use:

		Very complicated

> Phoenix code sharing model
	
	When we use this line in the new controller:
	use DiscussWeb, :controller

	We define this as a controller and defines all the behaviors of a controller.
	It gets the definitions from discuss_web.ex

	"Go and grab all of this modules":
		- Phoenix.Contoller
		- Discuss.Repo
		- Ecto
		- Ecto.Query
		- Discuss.Router.Helpers
		- Discuss.Gettext

	For a view:
		- Phoenix.View
		- Phoenix.Controller
		- Phoenix.HTML
		- Router.Helpers

	Model is not in discuss_web.ex but it says models get behavior from:
		- Ecto.Schema
		- Ecto
		- Ecto.Changeset
		- Ecto.Query

> Phoenix conn struct

	When we use this line:
	def new(conn, _params) do

	To debug:
    IO.puts "+++++"
    IO.inspect conn

	- conn: connection

	    IO.puts "+++++"
	    IO.inspect conn

	    All of the informations about the incoming request

	- params: parameters

	    IO.puts "+++++"
	    IO.inspect params

	    http://localhost:4000/topics/new?test=true
	    %{"test" => "true"}

	    Parse the url

> Models in Phoenix

	At this point of the course, models are outdated.

	From Phoenix documentation:

		https://hexdocs.pm/phoenix/ecto.html#the-schema

		To create a schema use this command:
		$ mix phx.gen.schema

		Example:

			Let's generate a User schema with name, email, bio, and number_of_pets fields:
			$ mix phx.gen.schema User users name:string email:string \
			bio:string number_of_pets:integer

			Remember to update your repository by running migrations:
			$ mix ecto.migrate

		$ mix phx.gen.schema Topic topics title:string

	It will produce this code:
	defmodule Discuss.Topic do
	  use Ecto.Schema
	  import Ecto.Changeset

	  schema "topics" do
	    field :title, :string

	    timestamps()
	  end

	  @doc false
	  def changeset(topic, attrs) do
	    topic
	    |> cast(attrs, [:title])
	    |> validate_required([:title])
	  end
	end

	The schema is self explanatory

	The changeset (function to validate the data) takes 2 arguments:
		- struct, which contains some data we want to store in db: topic
		- params, a hash that contains the properties we want to update the struct with: attrs

		cast produces a changeset, how we want to update the db
		Then send it to validators which adds errors to the changeset
		It return a struct called a changeset

	To test this:

		$ iex -S mix

		iex(2)> struct = %Discuss.Topic{}
		%Discuss.Topic{
		  __meta__: #Ecto.Schema.Metadata<:built, "topics">,
		  id: nil,
		  inserted_at: nil,
		  title: nil,
		  updated_at: nil
		}

		iex(3)> params = %{title: "Great JS"}
		%{title: "Great JS"}

		iex(4)> Discuss.Topic.changeset(struct, params)
		#Ecto.Changeset<
		  action: nil,
		  changes: %{title: "Great JS"},
		  errors: [],
		  data: #Discuss.Topic<>,
		  valid?: true
		>

		But if we pass an empty map for params, it will become invalid and add an error
		iex(5)> Discuss.Topic.changeset(struct, %{})   
		#Ecto.Changeset<
		  action: nil,
		  changes: %{},
		  errors: [title: {"can't be blank", [validation: :required]}],
		  data: #Discuss.Topic<>,
		  valid?: false
		>

	We could also have default arguments:
		def changeset(topic, attrs \\ %{}) do

		Like this it will default to an empty map

> setting up the form page

	Changeset + Form template = Usable form

	In the new function in TopicController, change the new method:

	  alias Discuss. Topic

	  def new(conn, params) do
	    struct = %Topic{}
	    params = %{}
	    changeset = Topic.changeset(struct, params)
	  end

	We added the alias to save use from repeating "Discuss." everytime

	Could refactor to:

	  def new(conn, params) do
	    changeset = Topic.changeset(%Topic{}, %{})
	  end

	  This generates an empty changeset

	Now create the form:

	First create views/potic_view.ex:
		Tell it to behae like a view:

		defmodule DiscussWeb.TopicView do
		  use DiscussWeb, :view
		end

	Now create a folder templates/topic
	And create a new.html.eex to match with the method 'new' and put placeholder text to see if everything works when accessing localhost:4000/topics/new

	I had this error: There are pending migrations for repo: `Discuss.Repo`. Try `mix ecto.migrate`

	So let's try:
	$ mix ecto.migrate

	I had to delete the table and run the migrations..

	And last thing, 
	"expected action/2 to return a Plug.Conn, all plugs must receive a connection (conn) and return a connection,"

	This tells use that we forgot to render the view in the controller

	  def new(conn, params) do
	    changeset = Topic.changeset(%Topic{}, %{})
	    render conn, "new.html"
	  end

	 And now everything works

> Generate the form 

	We could write a form from scratch with validation and all, but phoenix provides us with a generation tool:

		<%= form_for @changeset, topic_path(@conn, :create), fn f -> %>
		<% end %>

	"<%" allows us to write elixir code in our html, like: 
	<%= 1 + 1 %>

	form_for will generate an html form tag, with arguments.
	topic_path: where to send the form
	f: the form object

	Generate an input field
		<%= text_input f, :title, placeholder: "Title", class: "form-control "%>

	Generate a button:
		<%= submit "Save Topic", class "btn btn-primary" %>

	Also we have to pass the changeset variable:
		render conn, "new.html", changeset: changeset

	We can pass as many variables as we want here

	Now let's test in the browser the full code:
		<%= form_for @changeset, topic_path(@conn, :create), fn f -> %>
		  <div class="form-group">
		    <%= text_input f, :title, placeholder: "Title", class: "form-control "%>
		  </div>

		  <%= submit "Save Topic", class "btn btn-primary" %>
		<% end %>

	And we get this error:
	undefined function topic_path/2

	THis is because of the new version of phoenix.
	I need to use: 

		Routes.topic_path(@conn, :create)

> Get the data sent by the form

	Create a new route:

    	post "/topics", TopicController, :create

    	Notice the post instead of get

    And create a new method in Topic controller:

	  def create(conn, params) do
	    IO.inspect(params)
	    #render conn, "index.html"
	  end

	When I submit the form we can see in the log:

		%{
		  "_csrf_token" => "NDAFHiBeLzk2WB0-DHEQGCg9BAh3O0h9_aNzYfuwF4kixBBiBKEn-c8D",
		  "topic" => %{"title" => "My super new topic"}
		}

	The structure is very different from what we saw:

	iex> colors = %{red: "green"}
	iex> colors.red
	"green"

	iex> params = %{"topic" => "asdf"}
	iex> params.topic
	Error key :topic not found

	Need pattern matching:

	iex> %{"topic" => my_string} = params
	iex> my_string
	"asdf"

> Paths

	To get all of the routes available:

	$ mix phx.routes

	page_path  GET   /               DiscussWeb.PageController :index
    topic_path  GET   /topics/new    DiscussWeb.TopicController :new
    topic_path  POST  /topics        DiscussWeb.TopicController :create

    It gives us the general path name, the method, the url and the controller and method

> Process

	Params > changeset > Insert into db:
		- Success: Show topic list & show success
		- Failure: Show form & show error

	Phoenix relies mostly on Controller and Model
	Ecto relies on Changeset and Repo

	Repo module is the ultimate handle to the db

	Controller > changeset > Repo > Postgres

	We can directly call Repo in the controller

	  def create(conn, %{"topic" => topic}) do
	    changeset = Topic.changeset(%Topic{}, topic)
	    Repo.insert(changeset)
	  end

	The insert function will validate the data and make sure the changeset has the correct information before inserting it to the db

> Insert into the db using Repo

	def create(conn, %{"topic" => topic}) do
		changeset = Topic.changeset(%Topic{}, topic)

		case Discuss.Repo.insert(changeset) do
		  {:ok, post} -> IO.inspect(post)
		  {:error, changeset} -> IO.inspect(changeset)
		end
	end

	I had to call the Discuss.Repo instead of Repo.

	Here we test the cases returned by the insert method:
		- :ok
		- :error

	In the console we see the logs:
	[debug] Processing with DiscussWeb.TopicController.create/2
		  Parameters: %{"_csrf_token" => "Kh4MUSELPXtdLzRgG3ocECFFNgMuMgF2AOG5X3g5-CB7oINaK3wetjqO", "topic" => %{"title" => "JS Frameworks"}}
	  Pipelines: [:browser]
	[debug] QUERY OK db=10.3ms queue=5.7ms idle=1998.3ms
	INSERT INTO "topics" ("title","inserted_at","updated_at") VALUES ($1,$2,$3) RETURNING "id" ["JS Frameworks", ~N[2020-07-28 06:51:29], ~N[2020-07-28 06:51:29]]
	%Discuss.Topic{
	  __meta__: #Ecto.Schema.Metadata<:loaded, "topics">,
	  id: 1,
	  inserted_at: ~N[2020-07-28 06:51:29],
	  title: "JS Frameworks",
	  updated_at: ~N[2020-07-28 06:51:29]
	}

	Yay it works!

> Refactor (my own)

	I created a directory in /discuss/ called discuss_db
	Then in /discuss/discuss_db/topic.ex:

		defmodule DiscussDb.Repo.Topic do
		  import Ecto.Query, warn: false

		  alias Discuss.Repo

		  def create(topic) do
		    Repo.insert(topic)
		  end
		end

	So in my topic_controller.ex I can call this new module with a new alias

  	  alias DiscussDb.Repo

	  def create(conn, %{"topic" => topic}) do
	    changeset = Topic.changeset(%Topic{}, topic)

	    case Repo.Topic.create(changeset) do
	      {:ok, post} -> IO.inspect(post)
	      {:error, changeset} -> IO.inspect(changeset)
	    end
	  end

	I can even do better, add the changeset to this new file.
	So it has the whole Ecto ogic inside one file:

	"Ecto relies on Changeset and Repo"

		defmodule DiscussDb.Repo.Topic do
		  import Ecto.Query, warn: false

		  alias Discuss.Repo
		  alias Discuss.Topic

		  def create(topic) do
		    %Topic{}
		    |> Topic.changeset(topic)
		    |> Repo.insert()
		  end
		end

	And my controller is now clean:

	  def create(conn, %{"topic" => topic}) do
	    case Repo.Topic.create(topic) do
	      {:ok, post} -> IO.inspect(post)
	      {:error, changeset} -> IO.inspect(changeset)
	    end
	  end

> Error handling

	If we try to submit the form with an empty title, the log shows:

	[debug] Processing with DiscussWeb.TopicController.create/2
	  Parameters: %{"_csrf_token" => "IggcCyoKIiEgNkEHIkQhIDVAMwEJByV1IYWoS2xoPZ7PVwsQ_6rgS_UL", "topic" => %{"title" => ""}}
	  Pipelines: [:browser]
	#Ecto.Changeset<
	  action: :insert,
	  changes: %{},
	  errors: [title: {"can't be blank", [validation: :required]}],
	  data: #Discuss.Topic<>,
	  valid?: false
	>

	We see the error is defined and we get returned an Ecto.changeset
	So now we want to show the form again and show an error

	{:error, changeset} ->
        IO.inspect(changeset)

        conn
        |> put_flash(:error, String.capitalize(elem(changeset.errors[:title], 0)))
        |> render "new.html", changeset: changeset

    So I piped the connection and sent the chainset to the new.html template.
    We can show the error in the form with:

    	<%= error_tag f, :title %>

    But the best is to show a flash message.
    And pipe with the conn before the render.

    Type: :error
    Message: String.capitalize(elem(changeset.errors[:title], 0))

    	changeset.errors =
    		[title: {"can't be blank", [validation: :required]}]

    	changeset.errors[:title] =
    		{"can't be blank", [validation: :required]}

    	elem(changeset.errors[:title], 0) =
    		"can't be blank"

    	String.capitalize(elem) = 
    		"Can't be blank"

> Add css
	
	The error_tag has a class help-block by default
	In discuss/assets/app.scss

	.invalid-feedback {
	  color: red;
	  text-transform: capitalize;
	}

> get the list of topics

	From db
	  alias Discuss.Repo
	  alias Discuss.Topic

	  def get() do
	    Repo.all(Topic)
	  end

	In the controller:
	  def index(conn, _params) do
	    topics = Repo.Topic.get()
	    render conn, "index.html", topics: topics
	  end

	In the view as table:

		<div>
		  <table>
		    <tbody>
		      <%= for topic <- @topics do %>
		        <tr>
		          <td><%= topic.title %></td>
		        </tr>
		      <% end %>
		    </tbody>
		  </table>
		</div>

	Or in list

		<div>
		  <ul class="collection">
		    <%= for topic <- @topics do %>
		      <li class="collection-item"><%= topic.title %></li>
		    <% end %>
		  </ul>
		</div>

> Success handling
	
	Now if the topic was succesfully inserted, we want to display a message and redirect to the list of topics:

		{:ok, post} -> 
	        conn
	        |> put_flash(:info, "Topic created")
	        |> redirect(to: Routes.topic_path(conn, :index))

	Flash messages are displayed in /templates/layout/app.html.eex
	And here we have a redirect function with the path and specified method

> Add a link to homepage

      <li>
        <%= link("Topics", to: Routes.topic_path(@conn, :index)) %>
      </li>

      The link could also be

      <%= link to: Routes.topic_path(@conn, :index) do %>
      	<img />
      <% end %>

> Wildcard in router 

	To have wildcards in router you specify it with a colon

	    get "/topics/:id/edit", TopicController, :edit

	And the wildcard will be part of the parameters map

		def edit(conn, %{"id" => id}) do
    
  		end

> Edit the topic

	We need to get the topic and a changeset where nothing has changed yet
	I created two new method in my Repo.Topic

	  def get(id) do
	    Repo.get!(Topic, id)
	  end

	  def get_changeset(topic, attrs) do
	    Topic.changeset(topic, attrs)
	  end

	The controller will call both functions
	We send the topic id to be able to send the form to the correct path

	  def edit(conn, %{"id" => id}) do
	    topic = Repo.Topic.get(id)
	    changeset = Repo.Topic.get_changeset(topic, %{})

	    render conn, "edit.html", changeset: changeset, topic_id: topic.id
	  end

	Here is the form to edit:

	<%= form_for @changeset, Router.topic_path(@conn, :update, @topic_id), fn f -> %>
	  <div class="form-group">
	    <%= text_input f, :title, placeholder: "Title", class: "form-control "%>
	    <%= error_tag f, :title %>
	  </div>

	  <%= submit "Save Topic", class: "btn btn-primary" %>
	<% end %>

	The changeset is going to populate the title input with 'f'
	We leave a placeholder if the user deletes the whole string

> Update route and method
    
    put "/topics/:id", TopicController, :update

    To update, we need the old topic and the new topic

      From this:

      def update(conn, %{"id" => id, "topic" => topic}) do
	    old_topic = Topic.get(id)
	    changeset = Topic.get_changeset(old_topic, topic)
	  end

	  Refactored into this:

      def update(conn, %{"id" => id, "topic" => topic}) do
	    changeset = Topic.get(id) |> Topic.get_changeset(topic)
	  end

	  Then the update function takes the changeset as parameter to update that specific record.

	  	DiscussWeb.TopicController:

	  	  Topic.update(changeset)

	  	DiscussDb.Repo.Topic:

	      def update(changeset) do
		    Repo.update(changeset)
		  end

	Whole function:

	  def update(conn, %{"id" => id, "topic" => topic}) do
	    changeset = Topic.get(id) |> Topic.get_changeset(topic)

	    case Topic.update(changeset) do
	      {:ok, _topic} -> 
	        conn
	        |> put_flash(:info, "Topic updated!")
	        |> redirect to: Routes.topic_path(conn, :index)
	      {:error, changeset} ->
	        conn
	        |> put_flash(:error, String.capitalize(elem(changeset.errors[:title], 0)))
	        |> render conn, "edit.html", changeset: changeset
	    end
	  end

> Rest and Phoenix

	It is possible to replace all of those lines in the router:
		get "/", PageController, :index
	    get "/topics", TopicController, :index
	    post "/topics", TopicController, :create
	    get "/topics/new", TopicController, :new
	    get "/topics/:id/edit", TopicController, :edit
	    put "/topics/:id", TopicController, :update
	    delete "/topics/:id", TopicController, :delete

	By
		resources "/topics", TopicController

	If we follow the restful way it should all be fine
	The ressources parameter will be named "id"

> Delete

	Ecto functions:
		delete(struct or changeset, opts): 
			Delete a struct using its primary key

		delete!(struct or changeset, opts): 
			Same as delete/2 but returns the struct or raises if the changeset is invalid

		Same with get!

	--
	Controller

	  def delete(conn, %{"id" => id}) do
	    Topic.get!(id) |> Topic.delete!

	    conn
	    |> put_flash(:info, "Topic deleted")
	    |> redirect to: Routes.topic_path(conn, :index)
	  end

	DiscussDb.Repo.Topic

	  def delete!(topic) do
	    Repo.delete!(topic)
	  end

> Link tag

	By default it is goign to make a get request:
	
		<%= link "Edit", to: Routes.topic_path(@conn, :edit, topic) %>

	To specify another method, we have to specify it:

        <%= link "Delete", to: Routes.topic_path(@conn, :delete, topic.id), method: :delete %>

    It will generate:

    	<a data-csrf="HgUKFQByMH0bCg5gOWYYRjkTeBApb0gBuTAqyJj3kfx7MUJ7Se9vs788" data-method="delete" data-to="/topics/3" href="/topics/3" rel="nofollow">Delete</a>

> OAuth

	We will use a library to help us with OAuth

	Ueberauth
	https://github.com/ueberauth/ueberauth

	List of all strategies:
	https://github.com/ueberauth/ueberauth/wiki/List-of-Strategies

	Gitlab strategy:
	https://github.com/mtchavez/ueberauth_gitlab

	Install library:

	Add this in the mix.exs deps method
	{:ueberauth, "~> 0.6"}
	{:ueberauth_gitlab_strategy, "~> 0.2"}

	Then in the application method add the two modules to init:
	extra_applications: [:logger, :runtime_tools, :ueberauth, :ueberauth_gitlab_strategy]

	Then install the dependencies with:
	$ mix deps.get

	On gitlab.com, need to register an app
	App ID: 8a6b22d276c5efb7a49bb4f99933c557d4bd9978ae2291f65fa03b1e27e897e7
	Secret: 3b5e42b8740f4ece00e2516f6c9d6c0bbf0248887d748d4914e33897c6c12e44
	Callback URL: http://localhost:4000/auth/gitlab

	Then in config.esx add the ueberauth config:

	config :ueberauth, Ueberauth,
	  providers: [
	    identity: { Ueberauth.Strategy.Identity, [
	        callback_methods: ["POST"],
	        uid_field: :email,
	        nickname_field: :username,
	      ] },
	    gitlab: {Ueberauth.Strategy.Gitlab, [default_scope: "read_user"]},
	  ]

	Then just bellow:

	config :ueberauth, Ueberauth.Strategy.Gitlab.OAuth,
	  client_id: System.get_env("8a6b22d276c5efb7a49bb4f99933c557d4bd9978ae2291f65fa03b1e27e897e7"),
	  client_secret: System.get_env("3b5e42b8740f4ece00e2516f6c9d6c0bbf0248887d748d4914e33897c6c12e44"),
	  redirect_uri: System.get_env("http://localhost:4000/auth/gitlab")

	Setup the gitlab app. 
	If it was env variables we would call it with System.get_env()
	If it a string just put the string

> Create the auth controller

	contoller/auth_controller.ex

	defmodule Discuss.AuthController do
	  use Discuss.Web, :controller
	  plug Ueberauth

	  def callback(conn, params) do
	    IO.puts("=============")
	    IO.inspect(conn.assigns)
	    IO.puts("=============")
	    IO.inspect(params)
	    IO.puts("=============")
	  end
	end

> Router

	Create a new scope that will have to do with auth

	  scope "/auth", DiscussWeb do
	    pipe_through :browser
    	get "/gitlab", AuthController, :request
	  end

    pipe_through :browser means before we handle the request go through the pipeline :browser declared above

    The get /gitlab calls the premade method request
    We can replace /gitlab by :provider (for ex if we add facebook or google auth)

    And now add the callback route:

    	get "/:provider/callback", AuthController, :callback

> Callback from gitlab

	function Poison.decode!/1 is undefined (module Poison is not available)

	https://github.com/devinus/poison

	Poison is a new JSON library for Elixir focusing on wicked-fast speed without sacrificing simplicity, completeness, or correctness.

	Let's install poison:

		First, add Poison to your mix.exs dependencies:

		def deps do
		  [{:poison, "~> 3.1"}]
		end
		Then, update your dependencies:

		$ mix deps.get

	It works!

> Twitter Auth

	Let's try with twitter:

	https://developer.twitter.com/en/portal/projects-and-apps

	It works!

> conn.assigns

	It is used to stash information on the connection
	By convention it is what is used to add info to the connection
	Where we stash data as developers
	Sole area we have to share data within our app

	For example here, ueberauth added some new information

	%{
	  ueberauth_auth: %Ueberauth.Auth{
	    credentials: %Ueberauth.Auth.Credentials{
	      expires: false,
	      expires_at: nil,
	      other: %{},
	      refresh_token: "0b235fe74f60cd87f90e91ae63a6b8266a4edf6628b293b86aa860c3c085da60",
	      scopes: ["api read_user"],
	      secret: nil,
	      token: "d059e4093d761849da1b2c95be40c09b46a4d3b2538376d35d0eec2fd919bcc2",
	      token_type: "Bearer"
	    },
	    extra: %Ueberauth.Auth.Extra{
	      raw_info: %{
	        token: %OAuth2.AccessToken{
	          access_token: "d059e4093d761849da1b2c95be40c09b46a4d3b2538376d35d0eec2fd919bcc2",
	          expires_at: nil,
	          other_params: %{
	            "created_at" => 1596522382,
	            "scope" => "api read_user"
	          },
	          refresh_token: "0b235fe74f60cd87f90e91ae63a6b8266a4edf6628b293b86aa860c3c085da60",
	          token_type: "Bearer"
	        },
	        user: %{
	          "shared_runners_minutes_limit" => 2000,
	          "username" => "geoffb",
	          "twitter" => "https://twitter.com/__geoff___",
	          "job_title" => "",
	          "organization" => "",
	          "bio" => "",
	          "website_url" => "https://geoffb.gitlab.io",
	          "id" => 1165036,
	          "avatar_url" => "https://assets.gitlab-static.net/uploads/-/system/user/avatar/1165036/avatar.png",
	          "private_profile" => false,
	          "linkedin" => "https://www.linkedin.com/in/geoffroybaumier/",
	          "external" => false,
	          "two_factor_enabled" => false,
	          "bio_html" => "",
	          "web_url" => "https://gitlab.com/geoffb",
	          "skype" => "",
	          "theme_id" => 2,
	          "location" => "Valencia, Spain",
	          "last_sign_in_at" => "2020-07-29T07:01:53.480Z",
	          "state" => "active",
	          "created_at" => "2017-03-05T14:41:22.491Z",
	          "name" => "Geoff Baumier",
	          "color_scheme_id" => 2,
	          "identities" => [],
	          "can_create_group" => true,
	          "email" => "geoffroy.baumier@gmail.com",
	          "public_email" => "",
	          "extra_shared_runners_minutes_limit" => nil,
	          "confirmed_at" => "2017-03-05T14:42:11.585Z",
	          "projects_limit" => 100000,
	          "current_sign_in_at" => "2020-07-31T06:56:02.556Z",
	          "can_create_project" => true,
	          "last_activity_on" => "2020-08-04",
	          "work_information" => nil
	        }
	      }
	    },
	    info: %Ueberauth.Auth.Info{
	      birthday: nil,
	      description: nil,
	      email: "geoffroy.baumier@gmail.com",
	      first_name: nil,
	      image: "https://assets.gitlab-static.net/uploads/-/system/user/avatar/1165036/avatar.png",
	      last_name: nil,
	      location: "Valencia, Spain",
	      name: "Geoff Baumier",
	      nickname: "geoffb",
	      phone: nil,
	      urls: %{
	        web_url: "https://gitlab.com/geoffb",
	        website_url: "https://geoffb.gitlab.io"
	      }
	    },
	    provider: :gitlab,
	    strategy: Ueberauth.Strategy.Gitlab,
	    uid: 1165036
	  }
	}
	=============
	%{
	  "code" => "c94a4ba01d5103fa8701dcc595ecb83acb54335125330d4fe271397844bbd0ef",
	  "provider" => "gitlab"
	}
	=============

> Create user database

	Create a new migration: "add_users"

		$ mix ecto.gen.migration add_users
		* creating priv/repo/migrations/20200804070316_add_users.exs

	Go to priv/repo/migrations/20200804070316_add_users.exs

	  def change do
	    create table(:users) do
	      add :email, :string
	      add :provider, :string
	      add :token, :string

	      timestamps()
	    end
	  end

	Then run the migrations:

	  $ mix ecto.migrate

> User Schema

	Once the migration is executed we need a schema for the application to know that we have a user table available

	$ mix phx.gen.schema User users email:string provider:string token:string

	It will generate:

		defmodule Discuss.User do
		  use Ecto.Schema
		  import Ecto.Changeset

		  schema "users" do
		    field :email, :string
		    field :provider, :string
		    field :token, :string

		    timestamps()
		  end

		  @doc false
		  def changeset(user, attrs) do
		    user
		    |> cast(attrs, [:email, :provider, :token])
		    |> validate_required([:email, :provider, :token])
		  end
		end

	And it will also generate a migration:

		defmodule Discuss.Repo.Migrations.CreateUsers do
		  use Ecto.Migration

		  def change do
		    create table(:users) do
		      add :email, :string
		      add :provider, :string
		      add :token, :string

		      timestamps()
		    end

		  end
		end

	Ten to create the table:

	$ mix ecto.migrate

	So no need to do the step of creating a migrations

> Add a user to db from gitlab response

	In our callback function, we need to pattern match the whole object return (to auth)
	Then we can create the user data
	And create a changeset with an empty User struct

	  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, params) do
	    user_params = %{token: auth.credentials.token, email: auth.info.email, provider: auth.provider}
	    changeset = User.changeset(%User{}, user_params)
	  end

	But before inserting the user we need to check if the user exists (to no duplicate)

> Create a getByEmail function

	defp = private function (cannot be called by any other modules)
	(nice to do in auth controller)

	  defp getUserByEmail(email) do
	    Repo.get_by(User, email: email)
	  end

> The whole auth process

	defmodule DiscussWeb.AuthController do
	  use DiscussWeb, :controller
	  plug Ueberauth

	  alias Discuss.Repo
	  alias Discuss.User

	  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, %{"provider" => provider} = params) do
	    user_params = %{token: auth.credentials.token, email: auth.info.email, provider: provider}
	    changeset = User.changeset(%User{}, user_params)

	    IO.inspect(auth)

	    signin(conn, changeset)
	  end

	  defp signin(conn, changeset) do
	    case insertOrUpdateUser(changeset) do
	      {:ok, user} ->
	        conn
	        |> put_flash(:info, "You are connected")
	        |> put_session(:user_id, user.id)
	        |> redirect(to: Routes.page_path(conn, :index))

	      {:error, _reason} ->
	        conn
	        |> put_flash(:error, "Error signing in")
	        |> redirect(to: Routes.page_path(conn, :index))
	    end
	  end

	  defp insertOrUpdateUser(changeset) do
	    IO.inspect(changeset)
	    case getUserByEmail(changeset.changes.email) do
	      nil ->
	        Repo.insert(changeset)

	      user ->
	        {:ok, user}
	    end
	  end

	  defp getUserByEmail(email) do
	    Repo.get_by(User, email: email)
	  end
	end

	We put into session the user_id.
	What it does is that it creates a cookie with hashed inforamtion

	https://hexdocs.pm/phoenix/1.3.0-rc.1/sessions.html
	"Plug uses our secret_key_base value to sign each cookie to make sure it can’t be tampered with."

	The hash secret can be found in config/config.ex

> What is a plug

	Whenever a request comes through a scope then we pass it to the browser pipeline
	All of the plugs in the browser pipeline will run

	A plug does a transformation to the connection object
	All of the controllers are essentially plugs also (receive and must return a conn)

	Two different plugs: function and module plugs

	Module plug has an init function and a call
		The whole logic of the plug goes inside the call function
		init function is required and can stay blank, it takes one parameter (_params)
		Call function takes two arguments conn and _params (not like a controller params, it is what is passed from the init method)

	Function plug is like the function in our controller

> Create a plug

	Inside discuss_web/controllers/plugs
	Create set_user.ex

		defmodule Discuss.Plugs.SetUser do
		  import Plug.Conn
		  import Phoenix.Controller
		end

	Plug.Conn import helper functions for working with the conn object
	Phoenix.Controller imports 

> Set the user in the conn object

	To get the user_id set in the session (put_session) we use:

		user_id = get_session(conn, :user_id)

	cond do
		1+1 == 5 ->
			'five'
		2 + 2 == 20 ->
			'twenty'
		true ->
			true
	end

	What 'cond do' does is look at all of the expressions and return the first one that is true

	We will stash the user into the assign property of the conn

	To add key/value pair to socket assigns we use

		assign(socket, key, value)

	Then can get the data with

		conn.assigns[:user]
		conn.assigns.user => user struct

	THe assign method returns a conn object

> Whole plug

	defmodule Discuss.Plugs.SetUser do
	  import Plug.Conn
	  import Phoenix.Controller

	  alias Discuss.Repo
	  alias Discuss.User

	  def init(_params) do
	  end

	  def call(conn, _params) do
	    user_id = get_session(conn, :user_id)

	    cond do
	      user = user_id && Repo.get(User, user_id) ->
	        assign(conn, :user, user)
	        # conn.assigns.user => user struct
	      true -> 
	        assign(conn, :user, nil)
	    end
	  end
	end

> Let's use it

	In our router file
	Inside the pipeline we see that other plugs are atoms
	We are going to give a direct reference to the module

	  pipeline :browser do
	    plug :accepts, ["html"]
	    plug :fetch_session
	    plug :fetch_flash
	    plug :protect_from_forgery
	    plug :put_secure_browser_headers
	    plug Discuss.Plugs.SetUser
	  end

	Let's test this in our index function of page controller:

		IO.inspect(conn.assigns)

	And yes!

	%{
	  user: %Discuss.User{
	    __meta__: #Ecto.Schema.Metadata<:loaded, "users">,
	    email: "geoffroy.baumier@gmail.com",
	    id: 1,
	    inserted_at: ~N[2020-08-06 06:16:34],
	    provider: "gitlab",
	    token: "d059e4093d761849da1b2c95be40c09b46a4d3b2538376d35d0eec2fd919bcc2",
	    updated_at: ~N[2020-08-06 06:16:34]
	  }
	}

> Logout

	Add a route
	Make sure to put it above the :provider routes

		get "/signout", AuthController, :signout

	We want to delete the user data:

		configure_session(drop: true)

	Create a method in AuthController:

		  def signout(conn, _params) do
		    conn
		    |> configure_session(drop: true)
		    |> redirect(to: Routes.page_path(conn, :index))
		  end

	We could have updated the user to nil
	But in case we store other data in our session, it is best to drop everything

		<ul class="right">
          <li>
            <%= if @conn.assigns[:user] do %>
              <%= link("Logout", to: Routes.auth_path(@conn, :signout), class: "link") %>
            <% else %>
              <%=link(
                "Connect with Gitlab",
                to: Routes.auth_path(@conn, :request, "gitlab"),
                class: "link"
              ) %>
            <% end %>
          </li>
        </ul>

> Auth plug
	
	Create a new plug require_auth.ex

		defmodule Discuss.Plugs.RequireAuth do
		  def init(_params) do
		  end

		  def call(conn, _params) do
		    if conn.assigns[:user] do 
		      conn
		    else
		      conn
		      |> put_flash(:error, "You must be logged in")
		      |> redirect(to: Helpers.topic_path(conn, :index))
		      |> halt()
		    end
		  end
		end

	halt: 
		When using a plug and the plug finish its job, the conn is passed to the next plug.
		Here we don't want the user to continue

	Then we need a few imports:

		import Plug.Conn   : gives us halt
		import Phoenix.Controller   : put_flash & redirect
		alias DiscussWeb.Router.Helpers   : topic_path

> Implement it to the topic controller

	After the alias:

	  plug Discuss.Plugs.RequireAuth

	This will execute the plug before all methods of topic

	And this one will only execute the plug before thoses specified methods

	  plug Discuss.Plugs.RequireAuth when action in [:new, :create, :edit, :update, :delete]

> Assiociate user with a topic

	(database)
	The goal is to link a user with many topics

	Add a column user_id in topic:

		Create a migration file:

		$ mix ecto.gen.migration add_user_id_to_topics

			defmodule Discuss.Repo.Migrations.AddUserIdToTopics do
			  use Ecto.Migration

			  def change do
			    alter table(:topics) do
			      add :user_id, references(:users)
			    end
			  end
			end


		Note the references which reference another table

> Tell Phoenix about the connection

	Inside the user schema, we add one line: 

		[field...]
    	has_many :topics, Discuss.Topic

    Inside the topic schema:

    	belongs_to :user, Discuss.User

    Let's test this:

    $ iex -S mix
    iex(1)> Discuss.Repo.get(Discuss.User, 1)
    [debug] QUERY OK source="users" db=10.9ms decode=1.1ms queue=1.0ms idle=645.4ms
	SELECT u0."id", u0."email", u0."provider", u0."token", u0."inserted_at", u0."updated_at" FROM "users" AS u0 WHERE (u0."id" = $1) [1]
	%Discuss.User{
	  __meta__: #Ecto.Schema.Metadata<:loaded, "users">,
	  email: "geoffroy.baumier@gmail.com",
	  id: 1,
	  inserted_at: ~N[2020-08-06 06:16:34],
	  provider: "gitlab",
	  token: "d059e4093d761849da1b2c95be40c09b46a4d3b2538376d35d0eec2fd919bcc2",
	  topics: #Ecto.Association.NotLoaded<association :topics is not loaded>,
	  updated_at: ~N[2020-08-06 06:16:34]
	}

	We see a new field called topics!!! But it says it is not loaded
	They are not loaded by default

> Assiociate the user to a topic at the topic creation

	Ecto build_assoc()
	https://hexdocs.pm/ecto/Ecto.html#build_assoc/3

	iex> post = Repo.get(Post, 13)
	%Post{id: 13}
	iex> build_assoc(post, :comments)
	%Comment{id: nil, post_id: 13}

	In my DiscussDB.Repo.Topic
	Last create function:

	  def create(topic) do
	    %Topic{}
	    |> get_changeset(topic)
	    |> Repo.insert()
	  end

	New create function:

	  def create(topic, user) do
	    user
	    |> Ecto.build_assoc(:topics)
	    |> get_changeset(topic)
	    |> Repo.insert()
	  end

	Need to restart the server to test changes to Repo

> Change edit, delete topic 

	We need to make sure the owner to the topic can only edit or delete the topic
	In the topic index template we have this block:

	  <td>
	    <%= topic.title %>
	    <div class="right">
	      <%= link "Edit", to: Routes.topic_path(@conn, :edit, topic) %>&nbsp;
	      <%= link "Delete", to: Routes.topic_path(@conn, :delete, topic.id), method: :delete %>
	    </div>
	  </td>

	Now becomes this

	  <td>
        <%= topic.title %>
        <%= if @conn.assigns.user.id == topic.user_id do %>
          <div class="right">
            <%= link "Edit", to: Routes.topic_path(@conn, :edit, topic) %>&nbsp;
            <%= link "Delete", to: Routes.topic_path(@conn, :delete, topic.id), method: :delete %>
          </div>
        <% end %>
      </td>

    Now check the controller, not only the UI!

> create a plug

	Create a function plug: good when used inside a single module
	Inside topic controller:

	Under alias:

  	  plug :check_topic_owner when action in [:edit, :update, :delete]

  	At end of file:

	  def check_topic_owner(conn, _params) do
	    %{params: %{"id" => topic_id}} = conn

	    if Topic.get(topic_id).user == conn.assigns.user.id do
	      conn
	    else
	      conn
	      |> put_flash(:error, "You cannot edit that")
	      |> redirect(to: Routes.topic_path(conn, :index))
	      |> halt()
	    end
	  end

	The topic_id comes from the url as param (ex: /topics/15/edit)

> Create a new page to show topic content

	Create a new method: show

	  def show(conn, %{"id" => topic_id}) do
	    topic = Topic.get!(topic_id)
	    render conn, "show.html", topic: topic
	  end

	Show is the restful naming convention


	Now leet's createa a show.html.eex

		<h1><%= @topic.title %></h1>

	And make the topics clickable in the list of index.html.eex:

		<%= link topic.title, to: Routes.topic_path(@conn, :show, topic) %>

> Create a comment table

	$ mix phx.gen.schema Comments comments content:string user_id:references:users topic_id:references:topics

	It creates a migration

		defmodule Discuss.Repo.Migrations.CreateComments do
		  use Ecto.Migration

		  def change do
		    create table(:comments) do
		      add :content, :string
		      add :user_id, references(:users, on_delete: :nothing)
		      add :topic_id, references(:topics, on_delete: :nothing)

		      timestamps()
		    end

		    create index(:comments, [:user_id])
		    create index(:comments, [:topic_id])
		  end
		end

	Change the on_delete to :delete_all
	https://whatdidilearn.info/2018/03/18/more-about-ecto-and-ecto-queries.html

	And a Shema file

		defmodule Discuss.Comment do
		  use Ecto.Schema
		  import Ecto.Changeset

		  schema "comments" do
		    field :content, :string
		    belongs_to :user, Discuss.User 
		    belongs_to :topic, Discuss.Topic

		    timestamps()
		  end

		  @doc false
		  def changeset(comments, attrs \\ %{}) do
		    comments
		    |> cast(attrs, [:content])
		    |> validate_required([:content])
		  end
		end

	Then run the migration
	
		$ mix ecto.migrate

	Then we need to add the relation to the topic file:

		has_many :comments, Discuss.Comment

	And to the user file:

		has_many :comments, Discuss.Comment

> Channels in Phoenix (sockets)

	The socket is located in lib/discuss_web/channels/user_socket.ex
	Then ther is the js file /assets/js/socket.js

> Sockets

	The first thing we need is to declare the channel to which a client will attach
	The UserSocket module in user_socket.ex is like the router file for our channels

	In discuss_web/channels let's create a file "comments_channel.ex"

		defmodule DiscussWeb.CommentsChannel do
		  use DiscussWeb, :channel

		  def join() do

		  end

		  def handle_in() do

		  end
		end

	The join function is called when a new user joins our channel

	Then in our UserSocket module let's declare a channel:

		  # channel "room:*", DiscussWeb.RoomChannel
  		  channel "comments:*", DiscussWeb.CommentsChannel

  	The join function takes three parameters:

  		join(topic, auth_msg, socket)

  	The first, not to be confused with our topic is the widcard from comments:*, the name of the specific topic in the channel.
  	The second is a map of auth information 
  	And the last one is the socket itself
  	
  	https://hexdocs.pm/phoenix/Phoenix.Channel.html#c:join/3

  	Then we need to return either
	  {:ok, Phoenix.Socket.t()}
	  | {:ok, reply :: map(), Phoenix.Socket.t()}
	  | {:error, reason :: map()}

	But in most cases we will return the second tuple

	  def join(topic, _params, socket) do
	    IO.puts("=============================")
	    IO.inspect(topic)
	    IO.puts("=============================")
	    {:ok, %{hey: "there"}, socket}
	  end

	Finally to be able to test this we need to change the js file and change the channel the socket is trying to connect to:

		topic:subtopic  TO  comments:1

	And in app.js we need to uncomment the line that imports the socket
		import socket from "./socket"

	And that's it! Check the browser
	In the js console you can see joined successfully! and the {hey: "there"}
	And in the mix console we can see the comments:1

> Let's broadcast an event

	Handle in is used for the case
	It also takes three arguments and the return tuple is a little bit different

	  def handle_in(name, message, socket) do
	    IO.puts("=============================")
	    IO.inspect(topic)
	    IO.puts("=============================")
	    IO.inspect(message)
	    IO.puts("=============================")
	    {:reply, :ok, socket}
	  end

	Let's test this
	Add a button on the show.eex
		<button>Ping</button>

	In socket.js add a evenlistener

		document.querySelector('button').addEventListener('click', () => {
		  channel.push('comment:hello', {hi: "there"})
		})

	Now click on the button
	We can see in our mix console:

	=============================
	"comment:hello"
	=============================
	%{"hi" => "there"}
	=============================

> Get the comments of a topic: first let's get the id

	Let's encapsulate the socket creation inside of a function

		let socket = new Socket("/socket", {params: {token: window.userToken}})
		let channel

		socket.connect()

		const createSocket = (topicId) => {
		  channel = socket.channel(`comments:${topicId}`, {})
		  channel.join()
		    .receive("ok", resp => { console.log("Joined successfully", resp) })
		    .receive("error", resp => { console.log("Unable to join", resp) })
		}

		const ping = () => {
		  channel.push('comment:hello', {hi: "there"})
		  console.log("Ping sent")
		}

		window.ping = ping
		window.createSocket = createSocket

	In app.js we just need

		import "./socket"

	And in our show.html.eex

		<button onClick="window.ping()">Ping</button>
		<script>
		  document.addEventListener("DOMContentLoaded", () => {
		    window.createSocket(<%= @topic.id %>)
		  })
		</script>

> Fetch topic

	In our join function we know that the first argument "topic" will contain `comments:${topicId}` from our javascript.

	To get it from our join function we can do:

		def join("comments:" <> topic_id, _params, socket) do

	This pattern match from a string.
	The id will be a string, let's make it a number

		topic_id = String.to_integer(topic_id)

	Let's fetch the topic with the ID

		alias DiscussDb.Repo.Topic

	    def join("comments:" <> topic_id, _params, socket) do
		    topic_id = String.to_integer(topic_id)
		    topic = Topic.get!(topic_id)
		    IO.inspect(topic)

		    {:ok, %{hey: "there"}, socket}
	    end

> Add a comment

	Create a form in show.html.eex

		<div class="input-field">
		  <textarea class="materialize-textarea"></textarea>
		  <button class="btn">Add comment</button>
		</div>

	Then add some js to send the data

		document.querySelector('button').addEventListener('click', () => {
		  const content = document.querySelector('textarea').value
		  channel.push('comment:add', {content: content})
		})

	Now if I look at the message in handle_in I can see the comment

		def handle_in(name, message, socket) do
    		IO.inspect(message)
    		{:reply, :ok, socket}
  		end

		%{"content" => "onoasnonasdoinad"}
		[debug] HANDLED comment:add INCOMING ON comments:3 (DiscussWeb.CommentsChannel) in 96µs
  		Parameters: %{"content" => "onoasnonasdoinad"}

  	We need a way to pass down the topic to the handle_in function.
  	We can use the assigns just like on the conn.

	  def join("comments:" <> topic_id, _params, socket) do
	    topic_id = String.to_integer(topic_id)
	    topic = Topic.get!(topic_id)
	    IO.inspect(topic)

	    {:ok, %{hey: "there"}, assign(socket, :topic, topic)}
	  end

	Now we can get the topic in our handle_in method

	  def handle_in(name, message, socket) do
	    IO.inspect(message)
	    topic = socket.assigns.topic
	    IO.inspect(topic)
	    {:reply, :ok, socket}
	  end

	I wanted to keep the ping function so I added a case on the name:

		  def handle_in(name, message, socket) do
		    case name do
		      "comment:hello" ->
		        IO.inspect(message)
		        {:reply, :ok, socket}
		      "comment:add" ->
		        IO.inspect(message)
		        topic = socket.assigns.topic
		        IO.inspect(topic)
		        IO.inspect(name)
		        {:reply, :ok, socket}
		    end
		  end

	The whole handle_in method:

	  def handle_in(name, message, socket) do
	    case name do
	      "comment:hello" ->
	        IO.inspect(message)
	        {:reply, :ok, socket}
	      "comment:add" ->
	        %{"content" => content} = message
	        topic = socket.assigns.topic

	        changeset = topic
	          |> Ecto.build_assoc(:comments)
	          |> Comment.changeset(%{content: content})

	        case Discuss.Repo.insert(changeset) do
	          {:ok, comment} ->
	            {:reply, :ok, socket}
	          {:error, _reason} ->
	            {:reply, {:error, %{errors: changeset}}, socket}
	        end
	    end
	  end

> Alias

	If you alias two times the same module like so 

		alias Discuss.Repo
  		alias Discuss.Topic

  	You can concatenate to 

  		alias Discuss.{Repo, Topic}

> Send the list of comments through the socket

	PRELOAD
	In our join function, we want to replace the message with the list of comments

	  def join("comments:" <> topic_id, _params, socket) do
	    topic_id = String.to_integer(topic_id)
	    topic = Topic.get!(topic_id)

	    {:ok, %{hey: "there"}, assign(socket, :topic, topic)}
	  end

	We will preload the comments from the Topic request

	Usually you ould do something like 

		topic = Topic
			|> Repo.get(topic_id)
			|> Repo.preload(:comments)

	But we I can do better because I've setup a model file.
	Before:

	  def get!(id) do
	    Repo.get!(Topic, id)
	  end

	After:

	  def get!(id, preload \\ []) do
	    Repo.get!(Topic, id) |> Repo.preload(preload)
	  end 

	Now in my join function I can do

		topic = Topic.get!(topic_id, [:comments])

	And it doesn't affect the old code either!

	So here is the join function:

		  def join("comments:" <> topic_id, _params, socket) do
		    topic_id = String.to_integer(topic_id)
		    topic = Topic.get!(topic_id, [:comments])

		    {:ok, %{comments: topic.comments}, assign(socket, :topic, topic)}
		  end

	If we test this in the browser, there is an error about Json, poison encoding
	Well not poison but My version of phoenix is using Jason
	The problem is that it is trying to encode data that is only for Phoenix.

	We need to add one line of code in our comment model file:

		defmodule Discuss.Comment do
		  use Ecto.Schema
		  import Ecto.Changeset

		  @derive {Jason.Encoder, only: [:content, :inserted_at]}

		  schema "comments" do

> Display the comments

	Let's add a ul container:

		<div class="input-field">
		  <textarea class="materialize-textarea"></textarea>
		  <button class="btn">Add comment</button>
		  <ul class="collection"></ul>
		</div>

	Then in our socket.js

		.receive("ok", resp => {
	      console.log("Joined successfully", resp)
	      renderComments(resp.comments)
	    })

		const renderComments = comments => {
		  const renderedComments = comments.map(comment => {
		    return `<li class="collection-item">${comment.content}</li>`
		  })

		  document.querySelector('.collection').innerHTML = renderedComments.join('')
		} 

> Broadcast the new comment

	Notify all users
	Inside of the success case of inserting a comment

		case Discuss.Repo.insert(changeset) do
          {:ok, comment} ->
            {:reply, :ok, socket}

    We added one line of code:

    	{:ok, comment} ->
            broadcast!(socket, "comments:#{socket.assigns.topic.id}:new", %{comment: comment})
            {:reply, :ok, socket}

    broadcast!(socket, name_of_event, data)

    Now that we are broadcasting the new comment, let's display it

    In socket.js inside the createSocket function

    	channel.on(`comments:${topicId}:new`, renderComment)

		const renderComment = eventObject => {
		  document.querySelector('.collection').innerHTML += commentTemplate(eventObject.comment)
		}

		const commentTemplate = comment => {
		  return `<li class="collection-item">${comment.content}</li>`
		}

	And that is it!

> Authentication with sockets

	Phoenix doen't handle auth on sockets by default

	Idea:
	I - Generate a unique token and add it to layout
	II - Sockets boots up, sending user Token
	III - Server verifies token, adds user to socket

	I - Generate
	In the app.html.eex, in the header tag

	    <script>
	      <%= if @conn.assigns.user do %>
	        window.userToken = "<%= Phoenix.Token.sign(DiscussWeb.Endpoint, "key", @conn.assigns.user.id) %>"
	      <% end %>
	    </script>

	And if we refresh and inspect the source, we can find our script tag

		<script>
	        window.userToken = "SFMyNTY.g2gDYQFuBgA5W6IudAFiAAFRgA.r8Chx6u5Zoz4DQvQKJFSD3GGMP0wVGrlSXinQmM5fU4"
	    </script>

	II - Send the user token from js

	In socker.js we can see that there is already a line that sends the userToken:

		let socket = new Socket("/socket", {params: {token: window.userToken}})

	III - Receive it in phoenix socket and verifies the user

	In channels/user_socket.ex 
	We can see the connect function, it is used when we create a connection on the js side
	The first argument is params, which is used to send the params from js
	Then we verify it and assign the user_id to the socket

	  def connect(%{"token" => token}, socket, _connect_info) do
	    case Phoenix.Token.verify(socket, "key", token) do
	      {:ok, user_id} ->
	        {:ok, assign(socket, :user_id, user_id)}
	      {:error, _error} -> 
	        :error
	    end
	  end

	 Now let's add the id to the comment

	 Ecto.build_assoc can only be used once, but it can take multiple parameters

> Send the comment with the user to know who created the comment on our topic page

	Preload is used to only load one relation deep
	But we can do something like this:

		topic = Topic.get!(topic_id, [:comments])

		To 

		topic = Topic.get!(topic_id, [comments: [:user]])

		That's the only thing we have to do to load the users for each comments

	But in our Discuss.Comment module we told Jason to only get certain data, so let's add the user

		  @derive {Jason.Encoder, only: [:content, :user, :inserted_at]}

	But then we get the error again that says Jason cannot encode User.
	SO we need to specify this to the Discuss.User module also

  		@derive {Jason.Encoder, only: [:email]}

> Now there is one issue

	When adding a new comment and passing it to the broadcast, the user inside of the comment was not loaded

	So I found a solution :
    
    	cannot encode association :user from Discuss.Comment to JSON because the association was not loaded.

    => Need to fetch again to preload the user

		case Repo.insert(changeset) do
          {:ok, comment} ->
            new_comment = Repo.get!(Comment, comment.id) |> Repo.preload([:user])
            broadcast!(socket, "comments:#{socket.assigns.topic.id}:new", %{comment: new_comment})
            {:reply, :ok, socket}
          {:error, _reason} ->
            {:reply, {:error, %{errors: changeset}}, socket}
        end

> Finished!

> Change directory structe: the web directory got split into 2

	- discuss
		- Business logic
		- Db access
	- discuss_web
		- Code to communicate to the web

> Context

	Context: contains all the code taht describes how a particular system in our app works
	Create Accounts and Discussions context
	
	$ mix phx.gen.html Accounts User users email:string
	$ mix phx.gen.html Discussions Topic topic title:string
	$ mix phx.gen.html Discussions Comment comments content:string

	There would be a directory 

		discuss
			accounts
				accounts.ex
				user.ex
			discussions
				discussions.ex
				topic.ex
				comment.ex

	=> Read about context















